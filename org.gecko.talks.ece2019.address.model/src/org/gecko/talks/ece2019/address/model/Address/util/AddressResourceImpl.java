/**
 */
package org.gecko.talks.ece2019.address.model.Address.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.gecko.talks.ece2019.address.model.Address.util.AddressResourceFactoryImpl
 * @generated
 */
public class AddressResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public AddressResourceImpl(URI uri) {
		super(uri);
	}

} //AddressResourceImpl
