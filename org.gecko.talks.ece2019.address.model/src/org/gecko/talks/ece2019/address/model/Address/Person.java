/**
 */
package org.gecko.talks.ece2019.address.model.Address;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Person#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Person#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Person#getAddress <em>Address</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Person#getRelatives <em>Relatives</em>}</li>
 * </ul>
 *
 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getPerson()
 * @model
 * @generated
 */
public interface Person extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getPerson_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Person#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getPerson_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Person#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Address</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.talks.ece2019.address.model.Address.Address#getResidents <em>Residents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' reference.
	 * @see #setAddress(Address)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getPerson_Address()
	 * @see org.gecko.talks.ece2019.address.model.Address.Address#getResidents
	 * @model opposite="residents" required="true"
	 * @generated
	 */
	Address getAddress();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Person#getAddress <em>Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address</em>' reference.
	 * @see #getAddress()
	 * @generated
	 */
	void setAddress(Address value);

	/**
	 * Returns the value of the '<em><b>Relatives</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.talks.ece2019.address.model.Address.Person}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * All the relatives
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Relatives</em>' reference list.
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getPerson_Relatives()
	 * @model ordered="false"
	 *        annotation="http://www.eclipse.org/OCL/Collection nullFree='false'"
	 * @generated
	 */
	EList<Person> getRelatives();

} // Person
