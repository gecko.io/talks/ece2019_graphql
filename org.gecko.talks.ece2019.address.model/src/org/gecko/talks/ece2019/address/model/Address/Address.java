/**
 */
package org.gecko.talks.ece2019.address.model.Address;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Address</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An Address somebody lives at
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Address#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Address#getStreet <em>Street</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Address#getZipcode <em>Zipcode</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Address#getNumber <em>Number</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Address#getCity <em>City</em>}</li>
 *   <li>{@link org.gecko.talks.ece2019.address.model.Address.Address#getResidents <em>Residents</em>}</li>
 * </ul>
 *
 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getAddress()
 * @model
 * @generated
 */
public interface Address extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getAddress_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Address#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Street</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Street</em>' attribute.
	 * @see #setStreet(String)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getAddress_Street()
	 * @model
	 * @generated
	 */
	String getStreet();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Address#getStreet <em>Street</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Street</em>' attribute.
	 * @see #getStreet()
	 * @generated
	 */
	void setStreet(String value);

	/**
	 * Returns the value of the '<em><b>Zipcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zipcode</em>' attribute.
	 * @see #setZipcode(String)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getAddress_Zipcode()
	 * @model
	 * @generated
	 */
	String getZipcode();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Address#getZipcode <em>Zipcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zipcode</em>' attribute.
	 * @see #getZipcode()
	 * @generated
	 */
	void setZipcode(String value);

	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(String)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getAddress_Number()
	 * @model
	 * @generated
	 */
	String getNumber();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Address#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(String value);

	/**
	 * Returns the value of the '<em><b>City</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>City</em>' attribute.
	 * @see #setCity(String)
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getAddress_City()
	 * @model
	 * @generated
	 */
	String getCity();

	/**
	 * Sets the value of the '{@link org.gecko.talks.ece2019.address.model.Address.Address#getCity <em>City</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>City</em>' attribute.
	 * @see #getCity()
	 * @generated
	 */
	void setCity(String value);

	/**
	 * Returns the value of the '<em><b>Residents</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.talks.ece2019.address.model.Address.Person}.
	 * It is bidirectional and its opposite is '{@link org.gecko.talks.ece2019.address.model.Address.Person#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Who is living here
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Residents</em>' reference list.
	 * @see org.gecko.talks.ece2019.address.model.Address.AddressPackage#getAddress_Residents()
	 * @see org.gecko.talks.ece2019.address.model.Address.Person#getAddress
	 * @model opposite="address" ordered="false"
	 *        annotation="http://www.eclipse.org/OCL/Collection nullFree='false'"
	 * @generated
	 */
	EList<Person> getResidents();

} // Address
