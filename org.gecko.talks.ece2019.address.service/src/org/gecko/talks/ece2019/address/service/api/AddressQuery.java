/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.talks.ece2019.address.service.api;

import java.util.List;
import java.util.Optional;

import org.gecko.talks.ece2019.address.model.Address.Address;
import org.gecko.talks.ece2019.address.model.Address.Person;
import org.gecko.whiteboard.graphql.annotation.GraphqlArgument;
import org.gecko.whiteboard.graphql.annotation.GraphqlDocumentation;

import graphql.schema.DataFetchingEnvironment;

public interface AddressQuery{

	@GraphqlDocumentation("A Method to get Addresses by ID")
	public List<Address> getAddresses(@GraphqlArgument(optional = true, value = "id") String id);

	@GraphqlDocumentation("A Method to get Addresses by ID")
	public Optional<Address> getAddressOfPerson(@GraphqlArgument("person") Person person, DataFetchingEnvironment env);
	
}
