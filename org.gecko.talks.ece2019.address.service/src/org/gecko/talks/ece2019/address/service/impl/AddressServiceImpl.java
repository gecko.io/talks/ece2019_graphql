/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.talks.ece2019.address.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.gecko.talks.ece2019.address.model.Address.Address;
import org.gecko.talks.ece2019.address.model.Address.AddressFactory;
import org.gecko.talks.ece2019.address.model.Address.Person;
import org.gecko.talks.ece2019.address.service.api.AddressQuery;
import org.gecko.whiteboard.graphql.annotation.GraphqlQueryService;
import org.gecko.whiteboard.graphql.annotation.RequireGraphQLWhiteboard;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import graphql.schema.DataFetchingEnvironment;

@Component
@RequireGraphQLWhiteboard
@GraphqlQueryService
public class AddressServiceImpl implements AddressQuery{

	private List<Address> addresses = new LinkedList<>();
	private List<Person> persons = new LinkedList<>();
	
	@Activate
	public void activate() {
		Address addressGrandParents = createAddress("a1", "Grandparents Street", "42", "Ludwigsburg", "1234",addresses);
		Address addressParents = createAddress("a2", "Parents Street", "101010", "Stutgart", "1235",addresses);
		Address addressDaugther = createAddress("a3", "Kids Street", "2A", "Hamburg", "1237",addresses);
		
		Person grandMother = createPerson("p1", "Grandmother", persons);
		Person grandFather = createPerson("p2", "GrandFather", persons);
		Person mother = createPerson("p3", "Mother", persons);
		Person father = createPerson("p4", "Father", persons);
		Person daughter = createPerson("p5", "Daughter", persons);
		
		addRelatives(persons);
		
		addressGrandParents.getResidents().add(grandMother);
		addressGrandParents.getResidents().add(grandFather);
		
		addressParents.getResidents().add(mother);
		addressParents.getResidents().add(father);
		
		addressDaugther.getResidents().add(daughter);
		
	}
	
	@Deactivate
	public void deactivate() {
		addresses.clear();
		persons.clear();
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.talks.ece2019.address.service.api.AddressQuery#getAddresses(java.lang.String)
	 */
	@Override
	public List<Address> getAddresses(String id) {
		if(id == null) {
			return new ArrayList<>(addresses);
		} else {
			List<Address> result = new ArrayList<>(1);
			addresses.stream().filter(a -> id.equals(a.getId())).forEach(result::add);
			return result;
		}
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.talks.ece2019.address.service.api.AddressQuery#getAddressOfPerson(org.gecko.talks.ece2019.address.model.Address.Person)
	 */
	@Override
	public Optional<Address> getAddressOfPerson(Person person, DataFetchingEnvironment env) {
		
		return addresses.stream().filter(a -> {
			return a.getResidents().stream().filter(p -> {
				if(person.getId() != null) {
					return p.getId().equals(person.getId());
				} else {
					return p.getName().toLowerCase().equals(person.getName().toLowerCase());
				}
			}).findFirst().isPresent();
		}).findFirst();
	}
	
	/**
	 * Adds everybody except themself as relatives
	 * @param persons2
	 */
	private void addRelatives(List<Person> persons) {
		persons.forEach(p -> {
			persons.stream().filter(listPerson -> listPerson != p).forEach(p.getRelatives()::add);
		});
	}

	private Address createAddress(String id, String street, String number, String city, String zipcode, List<Address> cache) {
		Address address = AddressFactory.eINSTANCE.createAddress();
		address.setId(id);
		address.setStreet(street);
		address.setNumber(number);
		address.setCity(city);
		address.setZipcode(zipcode);
		cache.add(address);
		return address;
	}
	
	private Person createPerson(String id, String name, List<Person> cache) {
		Person p = AddressFactory.eINSTANCE.createPerson();
		p.setId(id);
		p.setName(name);
		cache.add(p);
		return p;
	}

}
